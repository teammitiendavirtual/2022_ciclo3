#Tuplas Son listas inmutables - No se pueden modificar
tupla = (4,"Hola",6.78,[1,2,3],4)

print(tupla[1:3])
print(4 in tupla)
print(tupla.index("Hola"))
print(tupla.count(4))
print(len(tupla))
#Para transformar una tupla en lista
lista = list(tupla)
print(lista)
#Para transformar una lista en tupla
lista1 = [1,3,5,7]
tupla1 = tuple(lista1)
print(tupla1)
#Agregue comentario el dia 4 de septiembre 2022 pero pienso quitarselo
